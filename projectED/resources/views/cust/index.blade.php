@extends('layouts.app')

@section('content')
    <div class="main py-4">
        <div class="card card-body border-0 shadow table-wrapper table-responsive">
            <h2 class="mb-4 h5">{{ __('Table Customer') }}</h2>

            <div>
            <button type="button" class="btn btn-sm btn-gray-800" data-bs-toggle="modal" data-bs-target="#modal">Tambah Data</button>
            </div>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="border-gray-200">{{ __('Nama') }}</th>
                        <th class="border-gray-200">{{ __('Jenis Kelamin') }}</th>
                        <th class="border-gray-200">{{ __('Alamat') }}</th>
                        <th class="border-gray-200">{{ __('No Hp') }}</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($data_cust as $cust)
                        <tr>
                            <td><span class="fw-normal">{{ $cust->nama_depan }}</span></td>
                            <td><span class="fw-normal">{{ $cust->jenis_kel }}</span></td>
                            <td><span class="fw-normal">{{ $cust->alamat }}</span></td>
                            <td><span class="fw-normal">{{ $cust->no_hp }}</span></td>

                        </tr>
                    @endforeach
                </tbody>
            </table>

            <!-- Modal Content -->
            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h2 class="h6 modal-title">Terms of Service</h2>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>With less than a month to go before the European Union enacts new consumer privacy laws for its citizens, companies around the world are updating their terms of service agreements to comply.</p>
                                                    <p>The European Union's General Data Protection Regulation (G.D.P.R.) goes into effect on May 25 and is meant to ensure a common set of data rights in the European Union. It requires organizations to notify users as
                                                        soon as possible of high-risk data breaches that could personally affect them.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary">Accept</button>
                                                    <button type="button" class="btn btn-link text-gray-600 ms-auto" data-bs-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


            <div
                class="card-footer px-3 border-0 d-flex flex-column flex-lg-row align-items-center justify-content-between">
            </div>
        </div>
    </div>
@endsection