<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustController extends Controller
{
    public function index()
    {
        $data_cust = \App\Models\cust::all();
        return view('cust.index',['data_cust' => $data_cust]);
    }
}
